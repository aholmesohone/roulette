import java.util.Scanner;
public class Roulette{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();

        int money = 1000;
        int betAmount =0;
        int betNumber =0;
        boolean gameEnd = false;
        boolean win = false;
        int input;

        while(!gameEnd){
            
            betAmount= bettingAmount(money); 
            money -= betAmount;
            System.out.println();

            betNumber= bettingNumber();
            System.out.println();

            wheel.spin();
            win = checkWin(betNumber, wheel);


            if(win){
                money += (betAmount*35);
                System.out.println("you won! and are now "+ (betAmount*35) +"$ richer.");
            }
            else{
                System.out.println("you lost, both your "+ betAmount +"$ and your dignity.");
            }
            System.out.println();

            System.out.println("Would you like to quit? type 1 to quit. 0 (or any other number) to continue.");
            input = scan.nextInt();
            if(input == 1){
                gameEnd = true;
            }

        }
        
        System.out.println("You left the casino with "+ money +"$");
        if(money <1000){
            System.out.println((1000-money) +"$ poorer.");
        }
        else if (money > 1000){
            System.out.println((money-1000) +"$ richer!");
        }
        else{
            System.out.println("exactly as much that you started with.");
        }

    }

    private static int bettingAmount(int current){
        Scanner scan = new Scanner(System.in);
        boolean valid = false;
        System.out.println("how much do you want to bet? you have "+ current + "$");
        int input =0;
        while(!valid){
            
            input = scan.nextInt();
            if(input <= 0 || input > current){
                System.out.println("either tried to bet 0 or bet more then you had, try again.");
                scan.next();
            }
            else{
                valid = true;
            }

            
        }

        return input;
    }

    private static int bettingNumber(){
        Scanner scan = new Scanner(System.in);
        boolean valid = false;
        System.out.println("on what number do you want to bet? (0-36)");
        int input =0;
        while(!valid){
            
            input = scan.nextInt();
            if(input < 0 || input > 36){
                System.out.println("bet was out of range. try again.");
                scan.next();
            }
            else{
                valid = true;
            }

            
        }

        return input;
    }

    private static boolean checkWin(int bet, RouletteWheel wheel){
        int result = wheel.getValue();
        System.out.println("The ball landed on: "+ result +"!");
        if(result == bet){
            return true;
        }
        else{
            return false;
        }
    }

}