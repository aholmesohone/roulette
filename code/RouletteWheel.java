import java.util.Random;
public class RouletteWheel {
    private Random rand;
    private int currentNumber;

    public RouletteWheel(){
        this.rand = new Random();
        this.currentNumber = 0;
    }

    public void spin(){
        this.currentNumber = rand.nextInt(37);
    }

    public int getValue(){
        return this.currentNumber;
    }
}
